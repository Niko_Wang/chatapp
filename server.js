var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');

new WebpackDevServer(webpack(config), {
  contentBase: "public",
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true
}).listen(3000, '0.0.0.0', function (err, result) {
  if (err) {
    return console.log(err);
  }

  console.log('Listening at http://localhost:3000/');
});

var http = require("https");
var express = require('express');
var fs = require('fs');
var firebase = require('firebase');
var message = require('./messages.json');

var app = express();

var config = {
  apiKey: "AIzaSyCuCXOvIfU3WdgkZ4lEsDy8CwZbbY9pz1A",
  authDomain: "my-test-project-60d18.firebaseapp.com",
  databaseURL: "https://my-test-project-60d18.firebaseio.com",
  projectId: "my-test-project-60d18",
  storageBucket: "my-test-project-60d18.appspot.com",
  messagingSenderId: "1091427122307"
};
firebase.initializeApp(config);

var database = firebase.database();

var server = app.listen(8000, function() {
    console.log('Server is running...');
});

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', "*");
  res.header('Access-Control-Allow-Methods','GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
})


app.get('/history', sendData);
function sendData(request, response) {
  var chatHistory = [];
  var ref = database.ref('chat_history');
  ref.on('value', (data) => {
    var item = data.val();
    var keys = Object.keys(item);
    for (var i=0; i < keys.length; i++) {
      var k = keys[i];
      var message = item[k].message;
      var time = item[k].time;
      var itemObject = {
        message: message,
        time: time
      }
      chatHistory.push(itemObject);
    }
  });
  response.send({chat_history: chatHistory});
  
}


app.get('/chat/:message?', saveData);
function saveData(request, response) {
  //var message = request.params.message;
  var ref = database.ref('chat_history');
  var d = new Date();
  var n = d.getTime();
  var data = {
    message: request.params.message,
    time: n
  }
  ref.push(data);
  //console.log(ref);
}