import React, {Component} from 'react';

export default class ChatBox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            input: ''
        }
    }

    render() {
        return (
            <form>
                <div>
                    <div className="col-sm-10">
                        <input className="form-control" id="textbox" onChange={event => this.setState({input: event.target.value})} />
                    </div>
                    <button className="btn btn-primary col-sm-2" onClick={() => {this.props.handler(this.state.input); document.getElementById('textbox').value = ''}}>Send</button>
                </div>
          </form>

        );
    }
}