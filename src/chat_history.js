import React, {Component} from 'react';

const ChatHistory = (props) => {
    const chatItem = props.history.map((message) => {
        return <div key={message.time}>{message.message}</div>
    });

    return (
        <div className="text-center">{chatItem}</div>
    );

}

export default ChatHistory