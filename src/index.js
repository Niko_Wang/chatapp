import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import Root from './root.js';
import { AppContainer } from 'react-hot-loader';

const render = Component => {
  ReactDOM.render(
  <AppContainer>
    <Component />
  </AppContainer>,
    document.getElementById('app')
  );
}

render(Root);
    
//ReactDOM.render(<ChatHistory />, document.getElementById('chathistory'));
//ReactDOM.render(<Chat />, document.getElementById('chat'));
//ReactDOM.render(<Search />,document.getElementById('home'));
//ReactDOM.render(<ShoppingList />,document.getElementById('app'));

if (module.hot) {
  module.hot.accept('./root.js', () => {
    const NextRootContainer = require('./root').default;
    render(NextRootContainer);
  });
}