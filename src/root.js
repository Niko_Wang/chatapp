import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import ChatBox from './chat_box.js';
import ChatHistory from './chat_history';

export default class Root extends React.Component {
    constructor(props) {
        super(props);
        
        this.state = {
            history: [],
            videos: [],
            handler: function(input) {
                if (input) {
                //this.setState({searchinput: event.target.value});
                    fetch('http://192.168.6.119:8000/chat/' + input)
                    .then(results => {
                        return results;
                    });
                }
                fetch('http://192.168.6.119:8000/history')
                .then(results => {
                    //console.log(results.json());
                    return results.json();
                }).then(data => {
                    this.setState({history: data.chat_history});
                    
                });

            }
         };
         this.handler = this.state.handler.bind(this);
    }

    /*componentWillMount() {
        fetch('http://localhost:8000/history')
        .then(results => {
            //console.log(results.json());
            return results.json();
        }).then(data => {
            console.log(data.chat_history);
            this.setState({history: data.chat_history});
            
        });
    }*/

    render() {
        if (!this.state.videos) return <p>Loading...</p>
        return(
            <div>
                <ChatHistory history={this.state.history} />
                <ChatBox handler={this.handler}/>
            </div>
        )
    }
}