var path = require("path");
var webpack = require("webpack");
module.exports = {
    //context: __dirname + "/src",
  
    /*entry: [
      'react-hot-loader/patch', // RHL patch
      './main.js' // Your appʼs entry point
    ],*/
    entry: [
      'react-hot-loader/patch',
      'webpack-dev-server/client?http://0.0.0.0:3000', // WebpackDevServer host and port
      'webpack/hot/only-dev-server', // "only" prevents reload on syntax errors
      './src/index.js' // Your appʼs entry point
    ],
  
    /*output: {
      filename: "bundle.js",
      path: __dirname + "public",
    },*/

    output: {
      path: path.resolve(__dirname, "public"),
      publicPath: "/",
      filename: "bundle.js"
    },

    resolve: {
        extensions: ['.js', '.jsx', '.json']
      },
      module: {
        loaders: [
          {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            //loaders: ["babel-loader"]
            loaders: ['react-hot-loader/webpack', 'babel-loader']
          }
        ]
      },
      plugins: [
        new webpack.HotModuleReplacementPlugin()
      ]
  };